#!/usr/bin/env python
# coding: utf-8

# In[27]:


get_ipython().run_line_magic('matplotlib', 'inline')
import numpy as np
import pandas as pd
from urllib.request import urlopen
import requests
from io import StringIO
import sklearn
import pickle
from sklearn.model_selection import train_test_split 
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier 
from sklearn.tree import DecisionTreeClassifier
from sklearn.multioutput import ClassifierChain
from sklearn import preprocessing
from sklearn.tree import tree 
from sklearn import svm
from sklearn.svm import LinearSVC
from sklearn.decomposition import PCA
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier 
from sklearn.metrics import confusion_matrix 
from sklearn.metrics import accuracy_score 
from sklearn.metrics import classification_report 
from sklearn.preprocessing import MultiLabelBinarizer
from skmultilearn.problem_transform import BinaryRelevance     ###ClassifierChain
from sklearn.naive_bayes import GaussianNB
import matplotlib.pyplot as plt
import seaborn as sn


# In[16]:


link="http://archive.ics.uci.edu/ml/machine-learning-databases/heart-disease/processed.cleveland.data"
names = ['age', 'sex', 'cp', 'trestbps', 'chol', 'fbs', 'restecg', 'thalach', 'exang', 'oldpeak', 'slope', 'ca', 'thal', 'heartdisease'] 
r = requests.get(link)
df = pd.read_csv(StringIO(r.text),  names = names)
le = preprocessing.LabelEncoder()

# df = pd.read_csv('DataSets/Heart_Disease_Data.csv', sep=',') 
heartDisease = df.iloc[:, :] 
print(heartDisease) 
print(heartDisease.head())
print(heartDisease.info()) 


# In[17]:


correlations = heartDisease.corr()
# plot correlation matrix
fig = plt.figure()
fig.set_size_inches(18.5, 10.5)
ax = fig.add_subplot(111)
cax = ax.matshow(correlations, vmin=-1, vmax=1)
fig.colorbar(cax)
ticks = np.arange(0,13,1)
ax.set_xticks(ticks)
ax.set_yticks(ticks)
ax.set_xticklabels(names)
ax.set_yticklabels(names)
plt.show()


# In[18]:


df.dropna(axis=0, inplace=True)
df.reset_index(drop=True, inplace=True)


# In[19]:


del heartDisease['slope']
del heartDisease['ca']
del heartDisease['thal']


# In[20]:


heartDisease = heartDisease.replace('?', -99999)
heartDisease.dtypes


# In[21]:


def normalize(heartDisease, toNormalize): #normalizes 
    result = heartDisease.copy()
    for item in heartDisease.columns:
        if (item in toNormalize):
            max_value = heartDisease[item].max()
            min_value = heartDisease[item].min()
            result[item] = (heartDisease[item] - min_value) / (max_value - min_value)
    return result
toNormalize = ['age', 'cp', 'trestbps', 'chol', 'thalach', 'oldpeak'] #columns to normalize
heartDisease = normalize(heartDisease, toNormalize)
heartDisease = heartDisease.dropna()
heartDisease.head()


# In[12]:


feature_cols = ['age', 'sex', 'cp', 'trestbps', 'chol', 'fbs' ,'restecg','thalach', 'exang','oldpeak']
X = heartDisease[feature_cols] # Features
# y = pd.Series(heartDisease['heartdisease'], name='heartdisease').unique() # Target variable
y = np.asarray(heartDisease['heartdisease'])  # Target variable

mlb = MultiLabelBinarizer()
y_indicator = mlb.fit_transform(y[:, None])


print(X)
print(y_indicator)
  


# In[ ]:





# In[22]:


#Spliting the data into test and train dataset 
X_train, X_test, y_train, y_test = train_test_split(X, y_indicator, test_size = 0.2, random_state = 42) 
# X, y = make_multilabel_classification(sparse = True, n_features=10, n_labels = 5, return_indicator = 'sparse', allow_unlabeled = False)


# In[36]:


Log_classifier = ClassifierChain(LogisticRegression())
# train
Log_classifier.fit(X_train, y_train)

# predict
predict_Reg = Log_classifier.predict(X_test)
print ('Accuracy Score for Logistic Regression for ClassifierChain :',accuracy_score(y_test, predict_Reg))
pickle.dump(Log_classifier, open('models/Log_classifierprediction.pkl', 'wb'))


# In[35]:


classifier_GnB = ClassifierChain(GaussianNB())
# train
classifier_GnB.fit(X_train, y_train)

# predict
predict_CC = classifier_GnB.predict(X_test)
print ('Accuracy Score for GaussianNB for ClassifierChain :',accuracy_score(y_test, predict_CC))
pickle.dump(classifier_GnB, open('models/classifier_GnBprediction.pkl', 'wb'))


# In[34]:


classifier_LogBR = BinaryRelevance(LogisticRegression())

# train
classifier_LogBR.fit(X_train, y_train)

# predict
predict_GnB = classifier_LogBR.predict(X_test)
print ('Accuracy Score for Logistic Regression for BinaryRelevance :',accuracy_score(y_test, predict_GnB))
pickle.dump(classifier_LogBR, open('models/classifier_LogBRprediction.pkl', 'wb'))


# In[33]:


classifier_BR = BinaryRelevance(GaussianNB())

# train
classifier_BR.fit(X_train, y_train)

# predict
predict_GnB = classifier_BR.predict(X_test)
print ('Accuracy Score for GaussianNB for BinaryRelevance :',accuracy_score(y_test, predict_GnB))
pickle.dump(classifier_BR, open('models/classifier_BRprediction.pkl', 'wb'))

