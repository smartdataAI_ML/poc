#!/usr/bin/env python
# coding: utf-8

# In[1]:


get_ipython().run_line_magic('matplotlib', 'inline')
import numpy as np
import pandas as pd
from urllib.request import urlopen
import requests
import pickle
from io import StringIO
import sklearn
from sklearn.model_selection import train_test_split 
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier 
from sklearn.tree import DecisionTreeClassifier 
from sklearn.tree import tree 
from sklearn import svm
from sklearn.svm import LinearSVC
from sklearn.decomposition import PCA
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier 
from sklearn.metrics import confusion_matrix 
from sklearn.metrics import accuracy_score 
from sklearn.metrics import classification_report 
from skmultilearn.problem_transform import BinaryRelevance
import matplotlib.pyplot as plt
import seaborn as sn


# In[2]:


#Importing the dataset 

link="http://archive.ics.uci.edu/ml/machine-learning-databases/heart-disease/processed.cleveland.data"
names = ['age', 'sex', 'cp', 'trestbps', 'chol', 'fbs', 'restecg', 'thalach', 'exang', 'oldpeak', 'slope', 'ca', 'thal', 'heartdisease'] 
r = requests.get(link)
df = pd.read_csv(StringIO(r.text),  names = names)
# df = pd.read_csv('DataSets/Heart_Disease_Data.csv', sep=',') 
heartDisease = df.iloc[:, :] 
print(heartDisease) 
print(heartDisease.head())
print(heartDisease.info()) 


# In[3]:


correlations = heartDisease.corr()
# plot correlation matrix
fig = plt.figure()
fig.set_size_inches(18.5, 10.5)
ax = fig.add_subplot(111)
cax = ax.matshow(correlations, vmin=-1, vmax=1)
fig.colorbar(cax)
ticks = np.arange(0,13,1)
ax.set_xticks(ticks)
ax.set_yticks(ticks)
ax.set_xticklabels(names)
ax.set_yticklabels(names)
plt.show()


# In[4]:


df.dropna(axis=0, inplace=True)
df.reset_index(drop=True, inplace=True)


# In[5]:


del heartDisease['thal']
del heartDisease['ca']
del heartDisease['slope']

heartDisease = heartDisease.replace('?', np.nan)
heartDisease.dtypes


# In[6]:


def normalize(heartDisease, toNormalize): #normalizes 
    result = heartDisease.copy()
    for item in heartDisease.columns:
        if (item in toNormalize):
            max_value = heartDisease[item].max()
            min_value = heartDisease[item].min()
            result[item] = (heartDisease[item] - min_value) / (max_value - min_value)
    return result
toNormalize = ['age', 'cp', 'trestbps', 'chol', 'thalach', 'oldpeak'] #columns to normalize
heartDisease = normalize(heartDisease, toNormalize)
heartDisease = heartDisease.dropna()
heartDisease.head()


# In[7]:


fig = plt.figure()
fig.set_size_inches(18.5, 10.5)
ax=fig.gca()
heartDisease.hist(ax=ax, bins=30)
plt.show()


# In[8]:


import plotly.graph_objs as go
import plotly.plotly as py
import plotly.tools as pt
plt.style.use('ggplot')
pt.set_credentials_file(username='titha', api_key='fejbdphzO6T3eHmKoZTN')

trace0 = go.Box(
    y=heartDisease['age'],
    name='age'
)
trace1 = go.Box(
    y=heartDisease['sex'],
    name='sex'
)
trace2 = go.Box(
    y=heartDisease['cp'],
    name='cp'
)
trace3 = go.Box(
    y=heartDisease['trestbps'],
    name='trestbps'
)
trace4 = go.Box(
    y=heartDisease['chol'],
    name='chol'
)
trace5 = go.Box(
    y=heartDisease['fbs'],
    name='fbs'
)
trace6 = go.Box(
    y=heartDisease['restecg'],
    name='restecg'
)
trace7 = go.Box(
    y=heartDisease['thalach'],
    name='thalach'
)
trace8 = go.Box(
    y=heartDisease['exang'],
    name='exang'
)
trace9 = go.Box(
    y=heartDisease['oldpeak'],
    name='oldpeak'
)
plotdata = [trace0, trace1, trace2, trace3, trace4, trace5, trace6, trace7, trace8, trace9]
py.iplot(plotdata)


# In[9]:


for i in range(1,5):
    heartDisease['heartdisease'] = heartDisease['heartdisease'].replace(i,1)
    
print(heartDisease['heartdisease'])    


# In[10]:


feature_cols = ['age', 'sex', 'cp', 'trestbps', 'chol', 'fbs' ,'restecg','thalach', 'exang','oldpeak']
X = heartDisease[feature_cols] # Features
y = heartDisease['heartdisease'] # Target variable

print(X)
print(y)
  


# In[11]:


#Spliting the data into test and train dataset 
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 42) 


# In[12]:


#Using the random forest classifier for the prediction 
classifier=RandomForestClassifier(random_state = 42, 
                                criterion='gini',
                                n_estimators = 2500,
                                max_features = 5) 
classifier=classifier.fit(X_train,y_train) 
predicted=classifier.predict(X_test) 
importancesRF = classifier.feature_importances_
indicesRF = np.argsort(importancesRF)[::-1]
indicesRF


# In[13]:


### Logistic Regression
logreg=LogisticRegression()
logreg.fit(X_train,y_train)
y_pred=logreg.predict(X_test)
logreg_coef = logreg.coef_[0]
print('\nLogisticRegression coefficients:', logreg_coef)
pickle.dump(logreg, open('models/logreg_prediction.pkl', 'wb'))


# In[14]:


### Decision Treee
dt = tree.DecisionTreeClassifier()
dt = dt.fit(X_train,y_train)
predict_dt = dt.predict(X_test)
pickle.dump(dt, open('models/dt_prediction.pkl', 'wb'))


# In[15]:


### K nearest neighbours
knn = KNeighborsClassifier(n_neighbors = 5)
knn.fit(X_train,y_train)
# 5. make prediction with the input from test data
predict_knn = knn.predict(X_test)
pickle.dump(knn, open('models/knn_prediction.pkl', 'wb'))


# In[16]:


### Support Vector Machine 
svm_clf=svm.SVC(kernel='linear')
svm_clf.fit(X_train,y_train)
y_pred_svm=svm_clf.predict(X_test)
pickle.dump(svm_clf, open('models/svm_clf_prediction.pkl', 'wb'))


# In[17]:


#Method to plot the graph for reduced Dimensions
def plot_2D(data,target,target_names):
    colors = cycle('rgbcmykw')
    target_ids = range(len(target_names))
    plt.figure()
    for i,c, label in zip(target_ids, colors, target_names):
        plt.scatter(data[target == i, 0], data[target == i, 1], c=c, label=label)
        plt.legend()
        plt.savefig('Problem 2 Graph')


# In[18]:


pca = PCA(n_components=2, whiten=True).fit(X)   # n denotes number of components to keep after Dimensionality Reduction
X_new = pca.transform(X)
modelSVM2 = svm.SVC(C = 0.6,kernel='rbf')
X_train1,X_test1,Y_train1,Y_test1 = train_test_split(X_new, heartDisease['heartdisease'], test_size = 0.2, train_size=0.1, random_state=0)
modelSVM2 = modelSVM2.fit(X_train1,Y_train1)
modelSVM2_predict=modelSVM2.predict(X_test1)
print("RBF score with split")
print(modelSVM2.score(X_test1,Y_test1))
pickle.dump(modelSVM2, open('models/modelSVM2_prediction.pkl', 'wb'))


# In[19]:


namesInd = names[:11]
print("Feature ranking:")

for f in range(10):
    i = f
    print("%d. The feature '%s' has a Gini Importance of %f" % (f + 1, namesInd[indicesRF[i]], importancesRF[indicesRF[f]]))


# In[20]:


#printing the results 
print ('Accuracy Score for Random Forest :',accuracy_score(y_test, predicted)) 
print ('Accuracy Score for LogisticRegression :',accuracy_score(y_test, y_pred)) 
print ('Accuracy Score for Decision Tree :',accuracy_score(y_test, predict_dt)) 
print ('Accuracy Score for KNN :',accuracy_score(y_test, predict_knn)) 
print ('Accuracy Score for SVM for Linear Kernel :',accuracy_score(y_test, y_pred_svm))
print ('Accuracy Score for SVM for rbf Kernel :',accuracy_score(Y_test1, modelSVM2_predict))
print('-----')

print ('Confusion Matrix for Random Forest :') 
print(confusion_matrix(y_test, predicted)) 
print ('Confusion Matrix for LogisticRegression :') 
print(confusion_matrix(y_test, y_pred)) 
print ('Confusion Matrix for Decision Tree :') 
print(confusion_matrix(y_test, predict_dt)) 
print ('Confusion Matrix for KNN :') 
print(confusion_matrix(y_test, predict_knn)) 
print ('Confusion Matrix for SVM for Linear Kernel :') 
print(confusion_matrix(y_test, y_pred_svm)) 
print ('Confusion Matrix for SVM for RBF Kernel :') 
print(confusion_matrix(Y_test1, y_pred_svm)) 

print('-----')

print ('Report for RandomForest: ') 
print (classification_report(y_test, predicted)) 
print ('Report for Logistic Regression: ') 
print (classification_report(y_test, y_pred)) 
print ('Report for Decision Tree: ') 
print (classification_report(y_test, predict_dt)) 
print ('Report for KNN :') 
print(classification_report(y_test, predict_knn)) 
print ('Report for SVM for Linear Kernel :') 
print(classification_report(y_test, y_pred_svm)) 

print('-----')

print(pd.crosstab(predicted, y_test, 
                  rownames=['Predicted Values'], 
                  colnames=['Actual Values']))


# In[21]:


cm=confusion_matrix(y_test,y_pred)
conf_matrix=pd.DataFrame(data=cm,columns=['Predicted:0','Predicted:1'],index=['Actual:0','Actual:1'])
plt.figure(figsize = (8,5))
sn.heatmap(conf_matrix, annot=True,fmt='d',cmap="YlGnBu")

