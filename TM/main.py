#!/usr/bin/env python
# coding: utf-8

# In[29]:


from flask import Flask, render_template, url_for, request
from sklearn.externals import joblib
import os
import numpy as np
import pickle

app = Flask(__name__, static_folder='static')

@app.route("/")
def index():
    return render_template('home.html')


# In[30]:


@app.route('/result', methods=['POST', 'GET'])
def result():
    age = int(request.form['age'])
    sex = int(request.form['sex'])
    trestbps = float(request.form['trestbps'])
    chol = float(request.form['chol'])
    restecg = float(request.form['restecg'])
    thalach = float(request.form['thalach'])
    exang = int(request.form['exang'])
    cp = int(request.form['cp'])
    fbs = float(request.form['fbs'])
    oldpeak = float(request.form['oldpeak'])

    x = np.array([age, sex, cp, trestbps, chol, fbs, restecg,
                  thalach, exang, oldpeak]).reshape(1,-1)

    # scaler_path = os.path.join(os.path.dirname(__file__), '../../models/knn_prediction.pkl')
    # scaler = None
    # with open(scaler_path, 'rb') as f:
    #     scaler = pickle.load(f)
    # print(scaler)
    # print(x)
    # # x = scaler.transform(x)

    model_path = os.path.join(os.path.dirname(__file__), '../../PythonTraining/models/Log_classifierprediction.pkl')
    clf = joblib.load(model_path)
    print(x)
    y = clf.predict(x)
    print(y)
    stage=np.flatnonzero(y)
    print(stage)

    if len(stage) == 0:
            ### No heart disease
        return render_template('nodisease.html')
    else:
        data=''
        for i in stage:
            data+=str(i)+','
            print('stage = ',data)
    data = data[:-1]
    return render_template('heartdisease.htm', stage=data)


# In[31]:




@app.route('/about')
def about():
    return render_template('about.html')


if __name__ == "__main__":
    app.run(port=4500,debug=True)

