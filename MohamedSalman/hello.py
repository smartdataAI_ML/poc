from flask import Flask
from flask import jsonify
from Stock import objstock
app = Flask(__name__)


@app.route("/", methods=['POST'])
def hello():
    """
    Feature to return response for stock api
    :author: Mohamed Salman
    :return: json
    """
    return jsonify({ 'status': 'success', 'data': objstock.graph() })


if __name__ == "__main__":
    app.run()