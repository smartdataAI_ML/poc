import pandas as pd
import os
import time
from datetime import datetime
import matplotlib.pyplot as plt
import re
import base64
from matplotlib import style
style.use("dark_background")


class Stock:
    def __init__(self):
        """
        Declaring all global variables
        """
        self.path = os.path.dirname(os.path.abspath(__file__)) + "/static/intraQuarter"
        self.graphName = "plot.png"
        pass

    def graph(self):
        """
        Reading all files and returning the graph base64 code
        """
        self.stats()
        with open(self.graphName, "rb") as image_file:
            encoded_string = base64.b64encode(image_file.read())
        return 'data:image/png;base64,' + encoded_string.decode('utf-8')

    def stats(self, gather="Total Debt/Equity (mrq)"):
        """
        Reading directory for calculating percentage change in stock and price
        """
        statspath = self.path + "/_KeyStats"
        # stock_list = [x[0] for x in os.walk(statspath)]
        df = pd.DataFrame(columns=["Date",
                                   "Unix",
                                   "Ticker",
                                   "DE Ratio",
                                   "Price",
                                   "stock_p_change"
                                   "SP500",
                                   "sp500_p_change",
                                   "Difference"])

        sp500_df = pd.read_csv(self.path + "/GSPC.csv")
        ticker_list = []

        stock_list = [statspath + '/aapl', statspath + '/abbv', statspath + '/abc', statspath + '/adi', statspath + '/aes', statspath + '/akam', statspath + '/aon']

        # for each_dir in stock_list[1:20]:
        for each_dir in stock_list:
            each_file = os.listdir(each_dir)
            ticker = each_dir.split("/")[-1]
            ticker_list.append(ticker)

            starting_stock_value = False
            starting_sp500_value = False

            if len(each_file) > 0:
                for file in each_file:
                    date_stamp = datetime.strptime(file, "%Y%m%d%H%M%S.html")
                    unix_time = time.mktime(date_stamp.timetuple())
                    full_file_path = each_dir + "/" + file
                    source = open(full_file_path, "r").read()
                    appended = False
                    stockAccessed = False

                    # calculating sp500 value
                    try:
                        sp500_date = datetime.fromtimestamp(unix_time).strftime('%Y-%m-%d')
                        row = sp500_df[(sp500_df.Date == sp500_date)]
                        sp500_value = float(row["Adj Close"])
                    except:
                        try:
                            sp500_date = datetime.fromtimestamp(unix_time - 259200).strftime('%Y-%m-%d')
                            row = sp500_df[(sp500_df.Date == sp500_date)]
                            sp500_value = float(row["Adj Close"])
                        except:
                            continue

                    # calculating stock price
                    try:
                        stock_price = float(source.split("</small><big><b>")[1].split("</b></big>")[0])
                        stockAccessed = True
                    except:
                        try:
                            stock_price = (source.split("</small><big><b>")[1].split("</b></big>")[0])
                            stock_price = re.search(r'(\d{1,8}\.\d{1,8})', stock_price)
                            stock_price = float(stock_price.group(1))
                        except:
                            pass

                    # calculating percentage change in stock and price
                    if not starting_stock_value:
                        starting_stock_value = stock_price
                    if not starting_sp500_value:
                        starting_sp500_value = sp500_value

                    stock_p_change = ((stock_price - starting_stock_value) / starting_stock_value) * 100
                    sp500_p_change = ((sp500_value - starting_sp500_value) / starting_sp500_value) * 100

                    # Setting dataframe rows
                    try:
                        value = float(source.split(gather + ':</td><td class="yfnc_tabledata1">')[1].split("</td>")[0])
                        df = df.append({"Date": date_stamp,
                                        "Unix": unix_time,
                                        "Ticker": ticker,
                                        "DE Ratio": value,
                                        "Price": stock_price,
                                        "stock_p_change": stock_p_change,
                                        "SP500": sp500_value,
                                        "sp500_p_change": sp500_p_change,
                                        "Difference": (stock_p_change - sp500_p_change)
                                        }, ignore_index=True)
                        appended = True
                    except Exception as e:
                        pass

                    try:
                        if (appended == False):
                            value = float(source.split(gather + ''':</td>
    <td class="yfnc_tabledata1">''')[1].split("</td>")[0])
                            df = df.append({"Date": date_stamp,
                                            "Unix": unix_time,
                                            "Ticker": ticker,
                                            "DE Ratio": value,
                                            "Price": stock_price,
                                            "stock_p_change": stock_p_change,
                                            "SP500": sp500_value,
                                            "sp500_p_change": sp500_p_change,
                                            "Difference": (stock_p_change - sp500_p_change)
                                            }, ignore_index=True)
                    except Exception as e:
                        pass

        for each_ticker in ticker_list:
            try:
                plot_df = df[(df['Ticker'] == each_ticker)]
                plot_df = plot_df.set_index(['Date'])
                plot_df['Difference'].plot(label=each_ticker)
                plt.legend()
            except:
                pass

        plt.savefig(self.graphName)
        plt.clf()
        return True


objstock = Stock()